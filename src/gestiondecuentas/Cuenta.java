/*
 * The MIT License
 *
 * Copyright 2020 runar.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package gestiondecuentas;

/**
 *
 * @author Rubén Martínez Cabello ad@martinead.es
 *
**/
public class Cuenta {
    private String titular;
    
    public String getTitular() { return this.titular; }
    public void setTitular(String titular) { this.titular = titular; }
    
    
    private Double cantidad;

    public Double getCantidad() { return this.cantidad; }
    public void setCantidad( Double cantidad) { this.cantidad = cantidad; }
    
    
    //public String toString() { return this.titular + " tiene " + this.cantidad.toString() + " euros en la cuenta"; }
    public String toString() { return this.getTitular() + " tiene " + this.getCantidad().toString() + " euros en la cuenta"; }


    
    // public Cuenta(String titular) { this.titular = titular; this.cantidad = 0.0; }
    public Cuenta(String titular) { this.setTitular(titular); this.setCantidad(0.0); }

    //public Cuenta(String titular, Double cantidad) { this.titular = titular; this.cantidad = cantidad; }
    public Cuenta(String titular, Double cantidad) { this.setTitular(titular); this.setCantidad(cantidad); }
    

/*    public void ingresar(Double cantidad)
    {
        if (cantidad >=0)
            this.cantidad += cantidad;
    }
*/    public void ingresar(Double cantidad)
    {
        if (this.getCantidad() >=0)
            this.setCantidad(this.getCantidad() + cantidad);
    }
    

/*    public void retirar(Double cantidad)
    {
        if (cantidad > this.cantidad)
            this.cantidad = 0.0;
        else
            this.cantidad -= cantidad;
    }
*/  public void retirar(Double cantidad)
    {
        if (cantidad > this.getCantidad())
            this.setCantidad(0.0);
        else
            this.setCantidad(this.getCantidad() - cantidad);
    }

}
