/*
 * The MIT License
 *
 * Copyright 2020 runar.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package gestiondecuentas;

/**
 *
 * @author runar
 */
public class CuentaApp {
    
    public static void main(String[] args) 
    {
        Cuenta alpe = new Cuenta("Alpe Formacion");
        Cuenta dani = new Cuenta("Daniel", 300.0);

        alpe.ingresar(300.0);
        dani.ingresar(400.0);
        
        alpe.retirar(500.0);
        dani.retirar(1000.0);
        
        System.out.println(alpe.toString());
        System.out.println(dani.toString());
    }
    
}
